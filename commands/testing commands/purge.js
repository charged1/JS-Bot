module.exports = {
  category: 'Testing',
  description: 'Purges messages.',

  maxArgs: 1,
  expectedArgs: '[amount]',

  slash: 'both',
  testOnly: 'true',

  callback: async ({ message, interaction, channel, args }) => {
    const amount = args.length ? parseInt(args.shift()) : 10

    if (message) {
      await message.delete()
    }

    const { size } = await channel.bulkDelete(amount, true)

    const reply = `Deleted ${size} message(s).`

    if (interaction) {
      return reply
    }

    channel.send(reply)
  },
}