const { MessageActionRow, MessageSelectMenu } = require('discord.js');

module.exports = {
  category: 'Testing',
  description: 'Replies with a dropdown',

  callback: ({ message }) => {
    
    const row = new MessageActionRow()
			.addComponents(
				new MessageSelectMenu()
					.setCustomId('select')
					.setPlaceholder('Nothing selected')
					.addOptions([
						{
							label: 'Select me',
							description: 'This is a description',
							value: 'first_option',
						},
						{
							label: 'You can select me too',
							description: 'This is also a description',
							value: 'second_option',
						},
					]),
			);

    return {
      custom: true,
      content: 'Pong!',
      components: [row]
    }

  },
}