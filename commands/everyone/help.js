const { MessageEmbed } = require('discord.js');
const { MessageActionRow, MessageSelectMenu } = require('discord.js');

module.exports = {
  category: 'Everyone',
  description: 'Replies with info on commands for the bot.',

  slash: 'both',
  testOnly: 'true',

  callback: async ({ message, interaction: msgInt, channel, text }) => {

    const embed = new MessageEmbed()
      .setTitle('FreeBot JS Commands')
      .setDescription('No prefix, this bot uses slash commands only. \n \n Please select a module with the selection menu. \n If a command does not work, please DM a staff member.')
      .setColor('RED')
      .setFooter('Developed by Charged')

    const row = new MessageActionRow()
			.addComponents(
				new MessageSelectMenu()
					.setCustomId('select')
					.setPlaceholder('Nothing selected')
					.addOptions([
						{
							label: '👮‍♂️ Moderation',
							description: 'Displays moderation commands.',
							value: 'moderation',
						},
					]),
			);

    return {
      custom: true,
      embeds: [embed],
      components: [row]
    };

    if (interaction.isSelectMenu()) {
      interaction.reply({ content: `You chose ${interaction.values[0]}` })
    }

  },
}