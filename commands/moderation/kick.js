module.exports = {
  category: 'Moderation',
  description: 'Kicks a member from the server',

  slash: 'both',
  testOnly: 'true',

  minArgs: 2,
  expectedArgs: '<user> <reason>',
  experctedArgsTypes: ['USER'],

  callback: ({ message, interaction, args }) => {
    const target = interaction ? interaction.mentions.members?.first() : interaction.options.getMember('user')
    if (!target) {
      return 'Please specify the user to kick.'
    } 

    if (!targed.kickable) {
      return {
        custom: true,
        content: 'The user specified is not kickable.'
      }
    }

    args.shift()
    const reason = args.join(' ')

    target.kick(reason)

    return {
      custom: true,
      content: '<@${target.id}> has been removed from the server.'
    }
  },
}