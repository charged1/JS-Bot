const { MessageEmbed } = require('discord.js');

module.exports = {
  category: 'FreeTechInfo',
  description: 'Reminds a user about patience.',

  slash: 'both',
  testOnly: 'true',

  callback: async ({ message, interaction, text }) => {

    const embed = new MessageEmbed()
      .setTitle('Reminder:')
      .setDescription('All work on this server is done by volunteers. If a FreeTech Expert is available, they will get to as soon as they can, as they are taking their own time to help you. Please be patient. If this continues, actions will be taken.')
      .setColor('RED')
      .setFooter('Thank You!')
      
    return embed
  },
}