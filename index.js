const Discord = require("discord.js")
const client = new Discord.Client({ intents: ["GUILDS", "GUILD_MESSAGES"] })
const keepAlive = require("./server")
const path = require('path')
const WOKCommands = require('wokcommands')

client.on('ready', () => {
  new WOKCommands(client, {
    commandsDir: path.join(__dirname, 'commands'),
    testServers: ['890754829276823562']
  })

  .setDefaultPrefix('js!')
})

client.on("ready", () => {
  console.log('Logged in as ${client.user.tag}!');
  client.user.setActivity('FreeBot', {type: `WATCHING`});
});

keepAlive()
client.login(process.env.TOKEN)